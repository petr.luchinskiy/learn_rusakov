'''
1) Попросите пользователя ввести 3 числа: год, месяц и число рождения.
2) Напишите ему, сколько секунд он уже живёт.
'''
import time
import datetime

year = int(input('Введите год'))
month = int(input('Введите месяца'))
day = int(input('Введите день'))
result = datetime.datetime.today()-datetime.datetime(year,month,day)
print('Вы уже живете ', result.total_seconds(), 'секунд')