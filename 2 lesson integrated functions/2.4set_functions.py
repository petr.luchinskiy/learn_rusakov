'''
1) Создайте 2 множества, состоящие из 10 случайных целых чисел от 1 до 25 (включая 1 и 25).
2) Выведите 3 множества: объединением этих двух множеств, их разницей и их пересечением.
'''
from random import randint

s1 = {randint(1,25) for i in range(10)}
s2 = {randint(0,25) for i in range(10)}


c = s1.union(s2) #объединение
c1 = s1.intersection(s2) #пересечение
c2 = s2.difference(s1) #разница

print(c, c1, c2)

print (s1, s2)