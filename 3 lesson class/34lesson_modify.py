'''
1) Сделайте у класса из предыдущего упражнения закрытыми все его поля.
2) Добавьте методы get и set для всех полей. Поскольку полей всего 4, то должно быть 4 метода get и 4 метода set.
3) Убедитесь, что доступа к полям уже нет за пределами класса.
4) Проверьте работу методов get и set.
5) Сделайте закрытый метод printlog(), в котором с помощью функции print() выводите значение переданного параметра.
6) В методах  set вызывайте метод printlog с параметром:  «Изменено свойство NAME» (для методов set). Вместо NAME должно быть подставлено имя соответствующего свойства.
'''
class rectangle():
    __width = 0
    __heigh = 0

    def __printlog(self, x):
        print('Запрошено свойство ', x)
    
    def getWidth(self):
        return self.__width
    
    def getHight(self):
        return self.__heigh

    def setWidth(self, x):
        self.__width = x
        self.__printlog(self.__width)
        

    def setHight(self, y):
        self.__heigh = y
        self.__printlog(self.__heigh)


    def __str__(self):
        return "«Прямоугольник с координатами (" + str(self.__heigh) +" "+  str(self.__width)+") "
    
    def __init__(self, heigh, width,) :
        self.__heigh = heigh
        self.__width = width
    
    def perimetr(self, heigh, width):
        return (self.__heigh + self.__width ) * 2
    
    def squeare(self):
        return self.__heigh*self.__width

x = rectangle(1, 9)
print(x.squeare())


