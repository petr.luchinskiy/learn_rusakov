'''
1) Узнайте, какое исключение появляется при делении числа на 0.
2) Попросите пользователя ввести 2 числа.
3) Выведите результат деления.
4) Перехватите исключение при делении на 0 и выведите пользователю в качестве результата слово «бесконечность».
'''
try:
    с = 3/0
except ZeroDivisionError:
    print('деление на 0')

a = int(input('введите первое число '))
b = int(input('введите второе число '))
try:
    if b==0:
        raise Exception('бесконечность')
    else:
        print('результат = ', a/b)
except Exception as exp:
    print(exp)
finally:
    print('программа завершена')
    exit(0)